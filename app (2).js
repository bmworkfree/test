$(document).ready(function(){

  		function btnTop() {

  			$(window).scroll(function(){
		        if ($(this).scrollTop() > 100) {
		            $('.scrollup').fadeIn();
		        } else {
		            $('.scrollup').fadeOut();
		        }
		     });
	          
		    $('.scrollup').click(function(){
		       	$("html, body").animate({ scrollTop: 0 }, 600);
		       	return false;
	    	});

  		}

  		function menuFix() {

  			$(window).scroll(function(){
		        if ($(this).scrollTop() > 0) {
		            $('.header').addClass('fixed');
		        } else {
		            $('.header').removeClass('fixed');
		        }
		    });

  		}


  		function styleSelect() {

        var years = new Array();
        var item = $('.style-select__item');
        var pane = '';

        function filt_on_data() {

          var j=0;
          $(pane+'.body-content__img-text').stop().fadeOut(0);
          $(pane+'.body-content__img-text').removeClass('active');
          for (var i = 0; i < years.length; i++) {
            if($(pane+'.body-content__img-text').is('[data-year="'+years[i]+'"]')) {
              $(pane+'.year-none').removeClass('active');

              $(pane+'.body-content__img-text[data-year="'+years[i]+'"').addClass('active');
              $(pane+'.body-content__img-text[data-year="'+years[i]+'"').fadeIn(400);
              j++;
            } else {
              if(j==0) {
                $(pane+'.year-none').addClass('active');
              }
            }
            
          }

        }

        function data_select_item() {

          item.click(function(){

            var thisVal = $(this).text();

            if(event.ctrlKey == true) {

              if ($(this).hasClass('active')) {
                  $(this).removeClass('active');
                  if (find_in_arr(years,thisVal)) {
                    years.splice(years.indexOf(thisVal),1);
                  }
              } else {
                $(this).addClass('active');
                if (!find_in_arr(years,thisVal)) {
                  years.push(thisVal);
                }
              }

            } else {

              $(".style-select__item").removeClass('active');
              $(this).addClass('active');
              years = [];
              $(this).closest('.style-select').find('.style-select__list').removeClass('open');
              years[0] = thisVal;

            }

            var defVal = 'ПО ГОДАМ';

            if(years.length == 0) {
              $(this).closest('.style-select').find('.style-select__selected-text').html(defVal);
            } else {
              $(this).closest('.style-select').find('.style-select__selected-text').html(years.join(','));
            }
     
            filt_on_data();

          });

        }

        function data_select_list() {

          function mobile_select() {

            if (self.screen) {
              width = $('body').width();
            }
            if(width>=768) {
              $(".style-select__selected").css('width',$(".style-select__selected").outerWidth());
            } else {
              $(".style-select__selected").css('width','100%');
            }
                
          }
          mobile_select();

            $(".style-select__selected").click(function(){

              var selected = $(this);
              var list = $(this).siblings('.style-select__list');

              if ($('.body-content__img-text').closest('.tab-pane').is('.tab-pane')) {
                pane = selected.closest('.style-select').attr('data-panel-id')+' ';
              }

              //определяем изначальную ширину для десктопа, и задаем ее чтобы не менлась при смене значений
              //для мобильных 100%

              if(!list.hasClass('open')) {
                list.addClass('open');
              } else {
                list.removeClass('open'); 
              }
              $(".style-select").mouseleave(function(){
                list.removeClass('open');
              });


              
            });

        }

        function current_projects() {
          $(".btn-more_projects,.btn-more_events").click(function(e){
            e.preventDefault();
             if ($('.body-content__img-text').closest('.tab-pane').is('.tab-pane')) {
                pane = $(this).closest('.tab-pane').attr('id')+' ';
                pane = '#'+pane;
              }
            if($(pane+'.body-content__img-text').is('[data-current-project="1"]')) {
              $(pane+'.body-content__img-text').stop().fadeOut(0);
              $(pane+'.body-content__img-text').removeClass('active');
              $(pane+'.year-none').removeClass('active');
              $(pane+'.body-content__img-text[data-current-project="1"').addClass('active');
              $(pane+'.body-content__img-text[data-current-project="1"').fadeIn(400);
          } else {
           
            $(pane+'.body-content__img-text').fadeOut(0);
            $(pane+'.year-none').addClass('active');
          }
          })
        }

        data_select_list();
        data_select_item();
        current_projects();
  			
  		}

      

      function find_in_arr(array, value) {

        for (var i = 0; i < array.length; i++) {
          if (array[i] == value) return true;
        }

        return false;

      }

      function findGetParameter(parameterName) {
          var result = null,
              tmp = [];
          location.search
              .substr(1)
              .split("&")
              .forEach(function (item) {
                tmp = item.split("=");
                if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
              });
          return result;
      }

      function tabs_link(){
        var params = findGetParameter('tab');
        if(params) {
          $(".tabs-project li:nth-child("+params+") a").click();
        }
        
      }


      function reset_filter_tabs() {
        $(".tabs-project li").click(function(){
           $('.year-none').removeClass('active');
              $('.body-content__img-text').addClass('active');
              $('.body-content__img-text').fadeIn(400);
              $('.style-select__selected .style-select__selected-text').text('ПО ГОДАМ')
        })
      }
      
	    
  		btnTop();
  		menuFix();
  		styleSelect();
      tabs_link();
      reset_filter_tabs();
 


	});
