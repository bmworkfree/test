let gulp       = require('gulp'), // Подключаем Gulp
	sass         = require('gulp-sass'), //Подключаем Sass пакет,
	browserSync  = require('browser-sync'), // Подключаем Browser Sync
	concat       = require('gulp-concat'), // Подключаем gulp-concat (для конкатенации файлов)
	uglify       = require('gulp-uglifyjs'), // Подключаем gulp-uglifyjs (для сжатия JS)
	cssnano      = require('gulp-cssnano'), // Подключаем пакет для минификации CSS
	rename       = require('gulp-rename'), // Подключаем библиотеку для переименования файлов
	del          = require('del'), // Подключаем библиотеку для удаления файлов и папок
	imagemin     = require('gulp-imagemin'), // Подключаем библиотеку для работы с изображениями
	pngquant     = require('imagemin-pngquant'), // Подключаем библиотеку для работы с png
	cache        = require('gulp-cache'), // Подключаем библиотеку кеширования
	autoprefixer = require('gulp-autoprefixer'),// Подключаем библиотеку для автоматического добавления префиксов
	//jade         = require('gulp-jade');
	rigger = require('gulp-rigger'), //работа с инклюдами в html и js
    wiredep = require('wiredep').stream,
    useref = require('gulp-useref'),
    gulpif = require('gulp-if'),
    ftp = require('gulp-ftp'),
    gutil = require('gulp-util');

//bower
gulp.task('bower', function () {
  gulp.src('app/html/index.html')
    .pipe(wiredep({
      directory : "app/bower_components/"
    }))
    .pipe(gulp.dest('app/html/'));
});

//html
gulp.task('htmlRigger', function(){
  return gulp.src('app/html/**/*.html')
  .pipe(rigger())
  .pipe(gulp.dest('app/'))
  .pipe(browserSync.reload({stream:true}))
});

//sass
gulp.task('sass', function(){ // Создаем таск Sass
	return gulp.src('app/sass/**/*.scss') // Берем источник
		.pipe(sass()) // Преобразуем Sass в CSS посредством gulp-sass
		.pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true })) // Создаем префиксы
		.pipe(gulp.dest('app/css')) // Выгружаем результата в папку app/css
		.pipe(browserSync.reload({stream: true})) // Обновляем CSS на странице при изменении
});

//img
gulp.task('img', function() {
	return gulp.src('app/images/**/*') // Берем все изображения из app
		.pipe(cache(imagemin({  // Сжимаем их с наилучшими настройками с учетом кеширования
			interlaced: true,
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
			use: [pngquant()]
		})))
		.pipe(gulp.dest('dist/img')); // Выгружаем на продакшен
});
//fonts
gulp.task('fonts', function() {
  return gulp.src('app/fonts/**/*')
  .pipe(gulp.dest('dist/fonts'))
})

//watch & browser-sync
gulp.task('browser-sync', function() { // Создаем таск browser-sync
	browserSync({ // Выполняем browserSync
		server: { // Определяем параметры сервера
			baseDir: 'app' // Директория для сервера - app
		},
		notify: false // Отключаем уведомления
	});
});

gulp.task('watch', ['browser-sync'], function() {
	gulp.watch('bower.json', ['bower']);
	gulp.watch('app/sass/**/*.scss', ['sass']); // Наблюдение за sass файлами в папке sass
	gulp.watch('app/html/**/*.html', ['htmlRigger']);
	gulp.watch('app/js/**/*.js', browserSync.reload);   // Наблюдение за JS файлами в папке js
});

gulp.task('default', ['watch']);


//build & clean
gulp.task('clean', function() {
	return del.sync('dist'); // Удаляем папку dist перед сборкой
});

gulp.task('build', ['clean', 'img', 'sass', 'fonts'],  function () {
    return gulp.src('app/html/*.html')
    	.pipe(rigger())
    	.pipe(useref())
        .pipe(gulpif('*.css', cssnano()))
        .pipe(gulpif('*.js', uglify()))
        .pipe(gulp.dest('dist'));
});

//deploy
gulp.task('sftp', function () {
    return gulp.src('dist/*')
        .pipe(sftp({
            host: 'demiweb.ftp.ukraine.com.ua',
            user: 'demiweb_argo',
            pass: 'mpc1k159',
            remotePath: '/'
        }));
});

gulp.task('ftp', function () {
    return gulp.src('dist/**/*')
        .pipe(ftp({
            host: 'demiweb.ftp.ukraine.com.ua',
            user: 'demiweb_argo',
            pass: 'mpc1k159',
        }))
        .pipe(gutil.noop());
});