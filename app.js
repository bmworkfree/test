$(document).ready(function() {

	$(".circ-link").click(function(e){
		e.preventDefault();
	});
	
	$(".circ").hover(function(e){
		var d = $(this).data('link');
		var block = $('.table-section__table-bl[data-link="'+d+'"]');
		if(block.hasClass('active')) {
			block.removeClass('active');
		} else {
			block.addClass('active');
		}
	})

	$(".room__i").hover(
		function(e){
			var d = $(this).find('.circ').data('link-room');
			var halfWidth = $(".room").width()/2;
			if($(this).position().left>halfWidth) {
				var l = $(this).position().left + $(this).width()/2 - 360 - 60;
			} else {
				var l = $(this).position().left + $(this).width()/2 + 60;
			}
			
			var t = $(this).position().top + $(this).height()/2;
		
			var h = $(this).find('.circ').data('room-h');
			var p = $(this).find('.circ').data('room-p');
			h = '<h5>'+h+'</h5>';
			p = '<p>'+p+'</p>';
			var bl = '<span class="room-append" style="left:'+l+'px; top:'+t+'px;">'+h+p+'</span>';
			$(".room__img-bl").find('.room-append').remove();
			$(".room__img-bl").append(bl);
			$(".room-append").fadeIn(300);
			/*
			var block = $('.table-section__table-bl[data-link="'+d+'"]');
			if(block.hasClass('active')) {
				block.removeClass('active');
			} else {
				block.addClass('active');
			}*/
	},function() {
		$(".room__img-bl").find('.room-append').remove();
	})
})